<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Importar</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:700,300);

html{
background-color: #585D61;
}

.frame {
	position: absolute;
	top: 50%;
	left: 50%;
	width: 400px;
	height: 400px;
	margin-top: -200px;
	margin-left: -200px;
	border-radius: 2px;
	box-shadow: 4px 8px 16px 0 rgba(0, 0, 0, 0.1);
	overflow: hidden;
	background: linear-gradient(to top right, #c6d9e8 0%, #d0d3e9 100%);
	color: #333;
	font-family: "Open Sans", Helvetica, sans-serif;
}

.center {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 300px;
	height: 282px;
	border-radius: 3px;
	box-shadow: 8px 10px 15px 0 rgba(0, 0, 0, 0.2);
	background: #fff;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	flex-direction: column;
}

.title {
	width: 100%;
	height: 50px;
	border-bottom: 1px solid #999;
	text-align: center;
}

h1 {
	font-size: 16px;
	font-weight: 300;
	color: #666;
}

.dropzone {
	<!-- //width: 100px; -->
	<!-- //height: 80px; -->
	<!-- border: 1px dashed #999; -->
	border-radius: 3px;
	text-align: center;
}

.upload-icon {
	margin: 25px 2px 2px 2px;
}

.upload-input {
	position: relative;
	top: -62px;
	left: 0;
	width: 100%;
	height: 100%;
	opacity: 0;
}

.btn {
	display: block;
	width: 140px;
	height: 40px;
	background: blue;
	color: #fff;
	border-radius: 3px;
	border: 0;
	transition: all 0.3s ease-in-out;
	font-size: 14px;
}

.btn:hover {
	background: rebeccapurple;
	box-shadow: 0 3px 0 0 deeppink;
}

[type="file"] {
  border: 0;
  clip: rect(0, 0, 0, 0);
  height: 1px;
  overflow: hidden;
  padding: 0;
  position: absolute !important;
  white-space: nowrap;
  width: 1px; 
}

[type="file"] + label {
  background-color: #585d61;
  border-radius: 4rem;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  font-family: 'Poppins', sans-serif;
  font-size: 1rem;
  font-weight: 700;
  height: 4rem;
  line-height: 4rem;
  padding-left: 2rem;
  padding-right: 2rem;
  transition: background-color 0.3s;
}
 
[type="file"]:focus + label,
[type="file"] + label:hover {
    background-color: #f15d22;
}
 
[type="file"]:focus + label {
  outline: 1px dotted #000;
  outline: -webkit-focus-ring-color auto 5px;
}

</style>
<script language="javascript" >
	function getname(){
		fName = document.getElementById("file").value;
		fullName = fName;
		cName = fullName.match(/[^\/\\]+$/);
		document.getElementById("nomf").value = cName;
	}  

	function validar(){
		fName = document.getElementById("file").value;
		fullName = fName;
		cName = fullName.match(/[^\/\\]+$/);
	if (fName != ""){
				ind = fName.indexOf(".");
				val = fName.substring(ind);
			if (val != ".xls"){
				alert("Error: El archivo debe tener extension '.xls'")
			}else{
				document.forms["mainform"].submit();
			}
		}  
		
	 window.open("querycount.asp", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=400,height=400");
	} 
	
	$(document).ready(function(){
	console.log("hola");
  <!-- $("button").click(function(){ -->

	function updateTime() {
	$.ajax(
	{url: "querycount.asp",
	success: function(result){
	console.log(result)
	  $("#div1").html(result);
    }});
  }
   <!-- alert("22"); -->
	<!-- } -->

	updateTime();
	setInterval(updateTime, 3000); 
  
  
  <!-- $("button").click(function(){ -->
    <!-- $.ajax( -->
	<!-- {url: "querycount.asp", -->
	<!-- success: function(result){ -->
	<!-- console.log(result) -->
	  <!-- $("#div1").html(result); -->
    <!-- }}); -->
  <!-- }); -->
});
</script>
	</head>

<body>
	<div class="frame">
		<br>
		<div class="center">
				<div align="center">
					<img src="img/logo.png" class="rounded" width="30%">
				</div>
				<h1>Actualizacion cliente</h1>
				<!-- <div id="div1">contador = 0 -->
				<div id="div1">contador = 0
				</div>
			<form  name="mainform" id="mainform" method="post" action="UploadFile.asp" enctype="multipart/form-data" autocomplete="OFF" />
			<div class="dropzone">
				<input type="file" class="upload-input" name="fichero" id="file" onchange="getname(value);" />
				<label for="file" />seleccionar archivo</label>
			</div>
			<br>
			<div align="center">
				<input type="text" name="nomf" id="nomf" />
			</div>
			<br>
			<div align="center">
				<button type="button" class="btn" name="uploadbutton" onClick="validar()">Cargar Archivo</button>
			</div>
			</form>
		</div>
	</div>
</body>

</html>
